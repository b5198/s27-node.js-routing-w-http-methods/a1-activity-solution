let http = require("http");

let products = [
	{
		name:"Iphone X",
		description: "Phone designed and created by Apple",
		price: 30000
	},
	{
		name:"Horizon Forbidden West",
		description: "Newest game for the PS4 and PS5",
		price: 4000
	},
	{
		name:" Razer Tiamat",
		description: "Headset from Razer",
		price: 3000
	}
];

http.createServer(function(req,res){
	console.log(req.url);
	console.log(req.method);
	if(req.url === "/" && req.method === "GET"){
		res.writeHead(200,{'Content-Type':'text/plain'});
		res.end("This is a response to a GET method request");
	} else if(req.url === "/" && req.method === "POST"){
		res.writeHead(200,{'Content-Type':'text/plain'});
		res.end("This is a response to a POST method request");
	} else if(req.url === "/" && req.method === "PUT"){
		res.writeHead(200,{'Content-Type':'text/plain'});
		res.end("This is a response to a PUT method request");
	} else if(req.url === "/" && req.method === "DELETE"){
		res.writeHead(200,{'Content-Type':'text/plain'});
		res.end("This is a response to a DELETE method request");
	} else if(req.url === "/products" && req.method === "GET"){
		res.writeHead(200,{'Content-Type':'application/json'});
		res.end(JSON.stringify(products));
	} else if(req.url === "/products" && req.method === "POST"){
		let requestBody = "";
		req.on('data',function(data){
			requestBody += data
		})
		req.on('end',function(){
			console.log(requestBody);
			requestBody = JSON.parse(requestBody);
			let newProduct = {
				name: requestBody.name,
				description: requestBody.description,
				price: requestBody.price
			}
			console.log(newProduct);
			products.push(newProduct);
			console.log(products);
		})
		res.writeHead(201,{'Content-Type':'application/json'});
		res.end(JSON.stringify(products));
	}
}).listen(8000);

console.log("Server is running on localhost:8000");